import { Image, Text, View } from '@tarojs/components'
import { Config, useDidShow, useState } from '@tarojs/taro'
import { StatResponse } from 'src/types/response'
import { CounterContainer } from './components/CountContainer/CountContainer'
import './index.less'
import Icon from '../../assets/card-tip-icon.png'
import StatBG from '../../assets/统计-bg.png'

export function Stat() {
  const [total, setTotal] = useState(0)

  useDidShow(async () => {
    const response = await Taro.cloud.callFunction({
      name: 'faas',
      data: { method: 'stat' },
    })

    const result = response.result as StatResponse
    setTotal(result.data.count)
  })

  const toReport = () => {
    Taro.navigateTo({
      url: '/pages/anonymous/index',
    })
  }

  const now = new Date()
  const date = `${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()}`

  return (
    <View className="container">
      <Image src={StatBG} className="background" />
      <View className="summary">
        <Text className="date">{date}</Text>
        <Text className="report-count">今日共 {total} 人上报健康状态</Text>
        <CounterContainer />
      </View>
      <View className="tip-container">
        <Image className="tip-icon" src={Icon} />
        <Text className="title">返校病患同行</Text>
        <Text onClick={toReport} className="report">
          信息上报
        </Text>
      </View>
      <View className="info-container">
        <Text className="info">
          共有 <Text className="num">32</Text> 人曾与确诊病患同行
        </Text>
        <View className="line" />
        <Text className="today">
          今日 <Text className="today-count">+{total}</Text> 人
        </Text>
      </View>
      <View className="tip-container">
        <Image className="tip-icon" src={Icon} />
        <Text className="title">校园疫情地图</Text>
      </View>
      <View className="building-container">
        <Text className="building">主教学楼</Text>
        <Text className="building-info">
          至今已有 <Text style={{ color: '#2969ee' }}>54</Text> 人去过，<Text style={{ color: '#ff0000' }}>1</Text>{' '}
          人曾与确诊病患同行
        </Text>
      </View>
      <View className="progress" />
      <View className="building-container">
        <Text className="building">艺术学院</Text>
        <Text className="building-info">
          至今已有 <Text style={{ color: '#2969ee' }}>54</Text> 人去过
        </Text>
      </View>
      <View className="progress" />
      <View className="building-container">
        <Text className="building">信息工程学院</Text>
        <Text className="building-info">
          至今已有 <Text style={{ color: '#2969ee' }}>54</Text> 人去过
        </Text>
      </View>
      <View className="progress" />
      <View className="patch" />
    </View>
  )
}

Stat.config = {
  navigationBarTitleText: '统计',
} as Config
